<?php
if( ! ini_get('date.timezone') )
{
    date_default_timezone_set('UTC');
}

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct() {
	parent::__construct();

	$this->load->helper('url');
	// Load form helper library
	$this->load->helper('form');

	$this->load->model('Login_model');
	// Load form validation library
	$this->load->library('form_validation');
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('templates/header.php');
	
		if($this->session->userdata('logged_in') == TRUE)
	    {
	    	$data['msg'] = $this->session->userdata('first_name');
	    	$data['load'] = $this->load->view('home/Greeting.php', $data, TRUE);
	    }
	    else
	    {
	    	$data['msg'] = "";
	    	$data['load'] = $this->load->view('login/registration_form.php', $data, TRUE);
	    }
	    //$text['mytext'] = "Welcome ";
		//$data['load'] = $this->load->view('login/registration_form.php', $text, TRUE);
		$this->load->view('home/home.php', $data);
	}

	public function test()
	{
		$this->load->view('templates/header.php');
	}

	public function installuser()
	{
		$this->load->view('templates/header.php');
		$this->Login_model->InstallUser();
	}

}
