<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if( ! ini_get('date.timezone') )
{
    date_default_timezone_set('UTC');
}

class Login extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->helper('url');
		// Load form helper library
		$this->load->helper('form');

		// Load form validation library
		$this->load->library('form_validation');

		// Load model
		$this->load->model('Login_model');

		$this->email->set_newline("\r\n");

	}

	private function checkLogin()
	{
		if($this->session->userdata('logged_in') == TRUE)
		{
			redirect('Welcome');
		}
		else
		{
			return 1;
		}
	}

	public function index()
	{
		if($this->checkLogin())
		{	
			$this->form_validation->set_rules('username', 'Username or Email', 'trim|required|min_length[4]');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]');
			if ($this->form_validation->run() == FALSE) 
			{
				$data['msg'] = "";
				$this->load->view('templates/header.php');
				$this->load->view('login/login_form', $data);
			}
			else
			{
				$clean = $this->security->xss_clean($this->input->post(NULL, TRUE));
		        $id = $this->Login_model->confirmUser($clean);
		        if($id)
		        {
			        foreach ($id->result() as $data)
			        {
			        	$first_name = $data->first_name;
			        	$last_name = $data->last_name;

				        	$newdata = array(
			                   'username'  => $data->username,
			                   'user_id'   => $data->user_id,
			                   'email'     => $data->email,
			                   'first_name'=> $data->first_name,
			                   'last_name' => $data->last_name,
			                   'welcome'   => TRUE,
			                   'logged_in' => TRUE
			                );

			                $this->session->set_userdata($newdata);
					        redirect('');
			    	}
			    }
			    else
			   	{
			   		$data['msg'] = "Username/Email and password Incorrect. Please try again.";
			   		$this->load->view('templates/header.php');
					$this->load->view('login/login_form', $data);
			   	}
			}
		}
	}

	public function user_logout()
	{
		$data['msg'] = "";
		$this->session->set_userdata('logged_in',FALSE);
		session_destroy();
		redirect('Welcome');
	}

	public function new_user_registration()
	{
		if($this->checkLogin())
		{
			$p1 = $this->input->post('password');
			$p2 = $this->input->post('passwordconf');
			if($p1 != $p2)
			{
				$data['msg'] = "The password and password confirmation need to match";
				$this->load->view('templates/header.php');
				$this->load->view('login/registration_form', $data);
			}
			else
			{
				$data['msg'] = "";
				$this->form_validation->set_message('is_unique', 'The %s is already taken.');
				// Check validation for user input in SignUp form
				$this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
				$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
				$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[4]|is_unique[user.username]');
				$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[user.email]');
				$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]');
				if ($this->form_validation->run() == FALSE) 
				{
					$this->load->view('templates/header.php');
					$this->load->view('login/registration_form', $data);
				}
				else
				{

					$clean = $this->security->xss_clean($this->input->post(NULL, TRUE));
		            $id = $this->Login_model->insertUser($clean);

					if ($id) 
					{
						$newdata = array(
			                   		'username'  => $this->input->post('username'),
			                   		'email'     => $this->input->post('email'),
			                   		'user_id'   => $id,
			                   		'first_name'=> $this->input->post('first_name'),
			                   		'last_name' => $this->input->post('last_name'),
			                   		'welcome'   => TRUE,
			                   		'logged_in' => TRUE
			                	);
						$this->session->set_userdata($newdata);
						/*$this->Login_model->insertUser_info($id);
						$token = $this->Login_model->insertToken($id);                                        
		            	$token .= "email*****";
		            	$token .= $id;
			            $qstring = $this->base64url_encode($token);                      
			            $url = site_url() . 'login/complete/' . $qstring;*/
			            $url = site_url();
			            $link = '<a href="' . $url . '">' . $url . '</a>'; 
			                       
			            $first_name = ucfirst($this->input->post('first_name'));
			            $last_name = ucfirst($this->input->post('last_name'));           
			            $message = '';                     
			            $message .= '<strong>Hi '.$first_name.' '.$last_name.',</strong><br><br>';
			            $message .= '<strong>You have registered up at our website with the username: '.$this->input->post('username').'</strong><br>';
			            $message .= '<strong>Click on the link below to visit the website:</strong><br>' . $link; 

			            $this->email->from('admin@pinoram.com' , 'Ron');
						$this->email->to($this->input->post('email')); 

				        $this->email->subject('You have registered at Login Test');
				        $this->email->message($message);  

				        $this->email->send();

			            //$data['msg'] = "Thank you for registering on Pinoram. A confirmation email has been sent.";
	                       
			            //$this->load->view('templates/header.php');
						//$this->load->view('login/login_form', $data);
						redirect('Login/Registered');
					} 
					else 
					{
						$data['msg'] = 'There is a problem registering your account...';
						$this->load->view('templates/header.php');
						$this->load->view('login/registration_form', $data);
					}
				}
			}
		}
	}

	public function registered()
	{
		$this->load->view('templates/header.php');
		$this->load->view('login/registered.php');
	}

	public function password_recovery()
	{
		if($this->session->userdata('logged_in') == TRUE)
		{
			$data['msg'] = "";
			$data['link'] = "login/password_conf";
			$this->load->view('templates/header.php');
			$this->load->view('login/password_conf', $data);
		}
	}

	public function password_conf(){
		$u_id = $this->session->userdata('user_id');
		$password = $this->input->post('password');
		$conf = $this->Login_model->confirm_password($u_id, $password);
		if($conf)
		{
			$data['msg'] = "";
			$this->session->set_userdata('password_reset', TRUE);
			$this->load->view('templates/header.php');
			$this->load->view('login/new_password', $data);
		}
		else
		{
			$data['msg'] = "Incorrect Password";
			$data['link'] = "login/password_conf";
			$this->load->view('templates/header.php');
			$this->load->view('login/password_conf', $data);
		}
	}


	public function new_password(){
		if($this->session->userdata('password_reset') == TRUE)
		{
			$this->form_validation->set_rules('new_password', 'Password', 'trim|required|min_length[5]');
			if ($this->form_validation->run() == FALSE) 
			{
				$data['msg'] = "";
				$this->load->view('templates/header.php');
				$this->load->view('login/new_password', $data);
			}
			elseif($this->input->post('new_password') == $this->input->post('new_password_conf'))
			{
				$new_password = $this->input->post('new_password');
				$conf = $this->Login_model->reset_password($new_password, $this->session->userdata('user_id'));
				if($conf)
				{
					if($this->session->userdata('logged_in') == TRUE)
					{
						$this->user_logout();
					}

					$data['msg'] = "Your password has been reset.";
				}
				else
				{
					$data['msg'] = "There was a problem resetting your password.";
				}
				$this->load->view('templates/header.php');
				$this->load->view('login/login_form', $data);
			}
			else{
				$data['msg'] = "Passwords do not match";
				$this->load->view('templates/header.php');
				$this->load->view('login/new_password', $data);
			}
		}
		else
		{
			$data['msg'] = "Sorry you are not allowed to reset password";
			$this->load->view('templates/header.php');
			$this->load->view('login/login_form', $data);
		}
	}


}

?>
