<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {

	public function __construct() {}

	public function InstallUser()
	{
		$this->load->dbforge();

		$fields = array(
			'user_id' => array(
				'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
			),
			'username' => array(
				'type' => 'VARCHAR',
                'constraint' => 30,
                'unique' => TRUE
			),
			'email' => array(
				'type' => 'VARCHAR',
                'constraint' => 100,
                'unique' => TRUE
			),
			'password' => array(
				'type' => 'VARCHAR',
                'constraint' => 64
			),
			'first_name' => array(
				'type' => 'VARCHAR',
                'constraint' => 64
			),
			'last_name' => array(
				'type' => 'VARCHAR',
                'constraint' => 64
			)
		);

		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('user_id', TRUE);
		$this->dbforge->add_key(array('username', 'email'));
		$this->dbforge->create_table('user', TRUE);
	}


	public function insertUser($data)
	{
		$string = array(
                'first_name'=>ucfirst($data['first_name']),
                'last_name'=>ucfirst($data['last_name']),
                'email'=>$data['email'],
                'password' =>password_hash($data['password'], PASSWORD_DEFAULT),
                'username' =>$data['username'],
            );
        $q = $this->db->insert_string('user',$string);             
        $this->db->query($q);
        return $this->db->insert_id();
	}

    /*public function insertUser_info($user_id)
    {
        $sql = "INSERT INTO user_info (user_id) VALUES ($user_id)";
        $this->db->query($sql);
    }*/

    public function confirmUser($data)
    {
        $username = $data['username'];
        $password = $data['password'];
        //$sql = "SELECT * FROM user WHERE '$username' IN(username, email) AND '$password' = password";
        $sql = "SELECT * FROM user WHERE '$username' IN(username, email)";
        $query = $this->db->query($sql);
        if ($query->num_rows() == 0)
        {
            return 0;
        }
        else
        {
            $result = $query->row();
            if (!password_verify($password, $result->password)) 
            {
                return 0;
            }
        }
        return $query;

    }

    public function confirm_password($u_id, $password){
        $sql = "SELECT * FROM user WHERE user_id = $u_id";
        $query = $this->db->query($sql);
        if ($query->num_rows() == 0)
        {
            return 0;
        }
        else
        {
            $result = $query->row();
            if (!password_verify($password, $result->password)) 
            {
                return 0;
            }
        }
        return $query;
    }

    public function reset_password($password, $id)
    {
        $pass = password_hash($password, PASSWORD_DEFAULT);
        $sql = "UPDATE user SET password = '$pass' WHERE user_id = $id";
        $query = $this->db->query($sql);
        return $query;
    }
}