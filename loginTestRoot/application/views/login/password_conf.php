<html>
	<body>
		<div id="home-background" >
			<div class="row" id="user-registration">
				<div class="card z-depth-5 col s12">
					<div class="error_msg" style="position: relative; color: red; text-align: center">
						<?php echo validation_errors();?>
					</div>
					<div style="position: relative; margin-top: 10px; color: black; text-align:center;">
						<?php echo $msg ?>
					</div>
			        <div class="card-content">
			        	<span class="card-title">Change Password</span>
			        	<hr/>
			        	<span>Please enter your current password.</span>
			        	<?php echo form_open($link); ?>
			        	<div class="row" style="margin-top: 5%; height: 100px">
			        		<div class="row">
					        	<div class="input-field col s12">
					          		<input type="password" id="password" name="password" class="validate"
					            			value="">
					          		<label for="password">Current Password</label>
					        	</div>
					        </div>
						</div>
						<div style="margin-top: -20px; width: 100%; position: relative;" >
			              <button style="width:80%; margin-left:10%;" class="btn waves-effect waves-light" type="submit" name="submit">Submit</button>
			            </div>
			        	<?php echo form_close(); ?>
					</div> 
				</div>
			</div>
		</div>
	</body>
</html>