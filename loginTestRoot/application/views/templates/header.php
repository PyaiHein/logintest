<?php defined('BASEPATH') OR exit('No direct script access allowed'); 


?>
<!DOCTYPE html>
<html>

   <style>
   #home-background {
         background: linear-gradient(rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)), url("/files/static_images/home.jpg");

         background-size: 100% 100%;
         height: 100%;
         background-repeat: no-repeat;
         margin-top: -22px;
      }
</style>
   <head>
   <!-- LOGO : TITLE -->
      <title>LoginTest</title>

      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>

      <link href="/assets/css/packery-docs.css" type="text/css" rel="stylesheet" media="screen,projection"/>
      <link href="/assets/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
      <link href="/assets/css/custom.css" type="text/css" rel="stylesheet" media="screen,projection"/>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <script type="text/javascript" src="/assets/js/jquery-3.1.1.min.js"></script> 
      <script type="text/javascript" src="/assets/js/materialize.min.js"></script>

      <?php if($this->session->userdata('logged_in') == TRUE) {?>
         <div class="navbar-fixed z-depth-5">   
            <nav >
               <div class="nav-wrapper">
                  <div class="row">
                     <div class="col s3 push-s1">
                        <a href="<?php echo base_url();?>" class="brand-logo left"><h4>Login Test</h4></a>

                     </div>
                     <div class="col s7 push-s1">
                     
                        <ul id="nav-mobile" class="right hide-on-small-and-down" style="margin-right: -30px">
                           <li><a href="<?php echo site_url();?>Login/password_recovery">Change Password</a></li>
                           <li><a href="<?php echo site_url();?>Login/user_logout">Logout</a></li>
                        </ul>

                        <ul id="nav-mobile" class="right hide-on-med-and-up">
                           <li><a style="position: relative; margin-right: -20px" class="dropdown-button" data-activates="dropdown1"><i class="material-icons right">menu</i></a></li>
                        </ul>
                     </div>
                  </div>   
               </div>
            </nav>
         </div>
      <?php } 

      else{ ?>
         <div class="navbar-fixed">   
            <nav>
               <div class="nav-wrapper">
                  <div class="row">
                     <div class="col s3 push-s1">
                        <a href="<?php echo base_url();?>" class="brand-logo left"><h4>Login Test</h4></a>
                     </div>
                     <div class="col s7 push-s1 pull-s1">
                        <ul id="nav-mobile" class="right hide-on-small-and-down" style="margin-right: -20px">
                           <li><a href="<?php echo site_url();?>Login">Login</a></li>
                        </ul>
                        <ul id="nav-mobile" class="right hide-on-med-and-up">
                           <li><a style="position: relative; margin-right: -20px" class="dropdown-button" data-activates="dropdown2"><i class="material-icons right">menu</i></a></li>
                        </ul>
                     </div>
                  </div>   
               </div>
            </nav>
         </div>
      <?php }?>
	
            <?php if($this->session->userdata('logged_in') == TRUE) {?>
         <ul id="dropdown1" class="dropdown-content">
            <li><a href="<?php echo site_url();?>Login/password_recovery">Change Password</a></li>
            <li><a href="<?php echo site_url();?>Login/user_logout">Logout</a></li>
         </ul>
      <?php } 

      else{ ?>
         <ul id="dropdown2" class="dropdown-content">
            <li><a href="<?php echo site_url();?>Login">Login</a></li>
         </ul>
      <?php } ?> 

   <script>

   $(function(){

      $(".button-collapse").sideNav();
   });

   $('.dropdown-button').dropdown({
      inDuration: 300,
      outDuration: 225,
      constrainWidth: false, // Does not change width of dropdown to that of the activator
      gutter: 0, // Spacing from edge
      belowOrigin: true, // Displays dropdown below the button
      alignment: 'right', // Displays dropdown with edge aligned to the left of button
      stopPropagation: false // Stops event propagation
    }
  );

  </script>



   </head>
</html>
